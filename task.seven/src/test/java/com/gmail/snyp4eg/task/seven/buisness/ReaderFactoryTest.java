package com.gmail.snyp4eg.task.seven.buisness;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.gmail.snyp4eg.task.seven.ui.reader.FileReader;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class ReaderFactoryTest {
    protected static final String FILES_PATH = "./src/test/resources/pathTest/pathTest.properties";
    protected static PropertyReader propertyReader;
    protected static ReaderFactory readerFactory;
    
    @BeforeAll
    public static void init() {
	propertyReader = new PropertyReader(FILES_PATH);
	readerFactory = new ReaderFactory(propertyReader);
    }
    
    @Test
    void shouldReturnFileReaderObjectThenGetFileReaderCalled() {
	assertEquals(readerFactory.getFileReader().getClass(), FileReader.class);
    }
    
    @Test
    void shouldReturnPropertyReaderObjectThenGetPathReaderCalled() {
	assertEquals(readerFactory.getPathReader().getClass(), PropertyReader.class);
    }
    
    @Test
    void shouldReturnPropertyReaderObjectThenGetQueryReaderCalled() {
	assertEquals(readerFactory.getQueryReader().getClass(), PropertyReader.class);
    }
    
    @Test
    void shouldReturnPropertyReaderObjectThenGetPropertyReaderCalled() {
	assertEquals(readerFactory.getPropertyReader().getClass(), PropertyReader.class);
    }
    
    void shouldReturnMenuReaderObjectThenGetMenuReaderCalled() {
	assertEquals(readerFactory.getMenuReader().getClass(), MenuReader.class);
    }
}

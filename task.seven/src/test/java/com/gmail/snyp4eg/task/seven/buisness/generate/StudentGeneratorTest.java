package com.gmail.snyp4eg.task.seven.buisness.generate;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.gmail.snyp4eg.task.seven.buisness.ReaderFactory;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class StudentGeneratorTest {
    protected static final String DATABASE_PROPERTY_PATH = "./src/test/resources/testDatabase.properties";
    protected static final String FILES_PATH = "./src/test/resources/pathTest/pathTest.properties";
    protected static ReaderFactory readerFactory;
    protected static StudentGenerator studentGenerator;
    protected static PropertyReader pathReader;
    
    @BeforeAll
    public static void init() {
	pathReader = new PropertyReader(FILES_PATH);
	readerFactory = new ReaderFactory(pathReader);
	studentGenerator = new StudentGenerator(readerFactory);
    }
    
    @Test
    void shouldReturnExpectedStudentListSizeThenGenerateMethodCalled() {
	assertEquals(20, studentGenerator.generate().size());
    }
}

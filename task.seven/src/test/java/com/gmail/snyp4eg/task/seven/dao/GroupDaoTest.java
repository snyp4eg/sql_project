package com.gmail.snyp4eg.task.seven.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.gmail.snyp4eg.task.seven.buisness.ConnectionFactory;
import com.gmail.snyp4eg.task.seven.buisness.ConnectionProperties;
import com.gmail.snyp4eg.task.seven.exceptions.DaoException;
import com.gmail.snyp4eg.task.seven.model.Group;
import com.gmail.snyp4eg.task.seven.ui.reader.FileReader;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class GroupDaoTest {
    protected static final String PROPERTY_PATH = "./src/main/resources/sql/query.properties";
    protected static final String QUERY_PATH = "./src/test/resources/sql/groupDaoTest.sql";
    protected static final String DATABASE_PROPERTY_PATH = "./src/test/resources/testDatabase.properties";
    protected static final String DELIMETER = "\n";
    protected static GroupDao groupDao;
    protected static ConnectionFactory connectionFactory;
    protected static ConnectionProperties connectionProperties;
    protected static FileReader fileReader;
    protected static PropertyReader reader;
    protected static String query;

    @BeforeAll
    public static void init() {
	connectionProperties = new ConnectionProperties();
	connectionFactory = new ConnectionFactory(connectionProperties.getConnectionProperties(DATABASE_PROPERTY_PATH));
	reader = new PropertyReader(PROPERTY_PATH);
	fileReader = new FileReader();
	groupDao = new GroupDao(connectionFactory, reader);
	query = getQuery();
	createTable(query);
    }

    private static void createTable(String tablesQuery) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(tablesQuery);) {
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    sqlEx.printStackTrace();
	} catch (DaoException ioEx) {
	    ioEx.printStackTrace();
	}
    }

    private static String getQuery() {
	List<String> list = fileReader.read(QUERY_PATH);
	StringJoiner stringJoiner = new StringJoiner(DELIMETER);
	list.stream().forEach(line -> {
	    stringJoiner.add(line);
	});
	return stringJoiner.toString();
    }

    @Test
    void shouldReturnExpectedCourseThenGetByIdCalled() {
	Group expectedCourse = new Group(1, "10-ER");
	assertEquals(groupDao.getById(1), expectedCourse);
    }

    @Test
    void shouldReturnExpectedCourseListThenGetAllCalled() {
	Group groupOne = new Group(1, "10-ER");
	Group groupTwo = new Group(2, "11-TY");
	List<Group> expectedGroupList = new ArrayList<>();
	expectedGroupList.add(groupOne);
	expectedGroupList.add(groupTwo);
	assertEquals(groupDao.getAll(), expectedGroupList);
    }

    @Test
    void shouldCreateNewDataThenInsertNewCourseCalled() {
	Group addGroup = new Group(3, "12-XX");
	groupDao.insert(addGroup);
	assertEquals(groupDao.getById(3), addGroup);
    }

    @Test
    void shouldUpdateExistDataThenUpdateCourseCalled() {
	Group updateGroup = new Group(1, "15-YY");
	groupDao.update(updateGroup);
	assertEquals(groupDao.getById(1), updateGroup);
    }

    @Test
    void shouldDeleteExistDataThenDeleteCourseCalled() {
	Group deleteGroup = new Group(2, "11-TY");
	groupDao.delete(deleteGroup);
	assertEquals(groupDao.getById(2), null);
    }
}

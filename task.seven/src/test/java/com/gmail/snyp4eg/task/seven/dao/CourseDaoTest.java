package com.gmail.snyp4eg.task.seven.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.gmail.snyp4eg.task.seven.buisness.ConnectionFactory;
import com.gmail.snyp4eg.task.seven.buisness.ConnectionProperties;
import com.gmail.snyp4eg.task.seven.exceptions.DaoException;
import com.gmail.snyp4eg.task.seven.model.Course;
import com.gmail.snyp4eg.task.seven.ui.reader.FileReader;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class CourseDaoTest {
    protected static final String PROPERTY_PATH = "./src/main/resources/sql/query.properties";
    protected static final String QUERY_PATH = "./src/test/resources/sql/courseDaoTest.sql";
    protected static final String DATABASE_PROPERTY_PATH = "./src/test/resources/testDatabase.properties";
    protected static final String DELIMETER = "\n";
    protected static CourseDao courseDao;
    protected static ConnectionFactory connectionFactory;
    protected static ConnectionProperties connectionProperties;
    protected static FileReader fileReader;
    protected static PropertyReader reader;
    protected static String query;

    @BeforeAll
    public static void init() {
	connectionProperties = new ConnectionProperties();
	connectionFactory = new ConnectionFactory(connectionProperties.getConnectionProperties(DATABASE_PROPERTY_PATH));
	reader = new PropertyReader(PROPERTY_PATH);
	fileReader = new FileReader();
	courseDao = new CourseDao(connectionFactory, reader);
	query = getQuery();
	createTable(query);
    }

    private static void createTable(String tablesQuery) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(tablesQuery);) {
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    sqlEx.printStackTrace();
	} catch (DaoException ioEx) {
	    ioEx.printStackTrace();
	}
    }

    private static String getQuery() {
	List<String> list = fileReader.read(QUERY_PATH);
	StringJoiner stringJoiner = new StringJoiner(DELIMETER);
	list.stream().forEach(line -> {
	    stringJoiner.add(line);
	});
	return stringJoiner.toString();
    }

    @Test
    void shouldReturnExpectedCourseThenGetByIdCalled() {
	Course expectedCourse = new Course(1, "algebra", "algebra");
	assertEquals(courseDao.getById(1), expectedCourse);
    }

    @Test
    void shouldReturnExpectedCourseListThenGetAllCalled() {
	Course courseOne = new Course(1, "algebra", "algebra");
	Course courseTwo = new Course(2, "math", "math");
	List<Course> expectedCoursesList = new ArrayList<>();
	expectedCoursesList.add(courseOne);
	expectedCoursesList.add(courseTwo);
	assertEquals(courseDao.getAll(), expectedCoursesList);
    }

    @Test
    void shouldCreateNewDataThenInsertNewCourseCalled() {
	Course addCourse = new Course(3, "art", "art");
	courseDao.insert(addCourse);
	assertEquals(courseDao.getById(3), addCourse);
    }

    @Test
    void shouldUpdateExistDataThenUpdateCourseCalled() {
	Course updateCourse = new Course(1, "neoarts", null);
	courseDao.update(updateCourse);
	assertEquals(courseDao.getById(1), updateCourse);
    }

    @Test
    void shouldDeleteExistDataThenDeleteCourseCalled() {
	Course deleteCourse = new Course(2, "math", "math");
	courseDao.delete(deleteCourse);
	assertEquals(courseDao.getById(2), null);
    }
}

package com.gmail.snyp4eg.task.seven.buisness;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ConnectionFactoryTest {
    protected static final String DATABASE_PROPERTY_PATH = "./src/test/resources/testDatabase.properties";
    protected static ConnectionFactory connectionFactory;
    protected static ConnectionProperties connectionProperties;
    
    @BeforeAll
    public static void init() {
	connectionProperties = new ConnectionProperties();
	connectionFactory = new ConnectionFactory(connectionProperties.getConnectionProperties(DATABASE_PROPERTY_PATH));
    }
    
    @Test
    void shouldReturnNotEmptyStringThenGetQueryCalled() {
	assertNotNull(connectionFactory.createConnection());
    }
}

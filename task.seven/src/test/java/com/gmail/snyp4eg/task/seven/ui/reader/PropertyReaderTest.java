package com.gmail.snyp4eg.task.seven.ui.reader;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PropertyReaderTest {
    protected static final String QUERY_PATH = "./src/test/resources/query.properties";
    protected static final String KEY = "select";
    protected static final String EXPECT_QUERY = "SELECT * FROM students";
    protected static PropertyReader propertyReader;
    
    @BeforeAll
    public static void init() {
	propertyReader = new PropertyReader(QUERY_PATH);
    }
    
    @Test
    void shouldReturnExpectedQueryThenKeyIsCalled() {
	assertEquals(propertyReader.getProperty(KEY), EXPECT_QUERY);
    }
    
}

package com.gmail.snyp4eg.task.seven.buisness.menu;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import com.gmail.snyp4eg.task.seven.buisness.services.StudentService;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class DeleteStudentTest {
    protected static DeleteStudent deleteStudent;
    protected static StudentService studentService;
    protected static MenuReader menuReader;
    protected static InOrder inOrder;
    
    @BeforeAll
    public static void init() {
	studentService = mock(StudentService.class);
	menuReader = mock(MenuReader.class);
	deleteStudent = spy(new DeleteStudent(studentService, menuReader));
    }
    
    @Test
    void shouldInternalMethodCalledByOrderThenExecuteMethodCalled() {
	inOrder = inOrder(menuReader, studentService);
	deleteStudent.execute();
	inOrder.verify(menuReader).getSubMenuPoint(Mockito.anyString());
	inOrder.verify(studentService).deleteStudent(Mockito.anyInt());
    }
}

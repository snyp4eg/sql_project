package com.gmail.snyp4eg.task.seven.ui.reader;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class FileReaderTest {
    protected static final String FILE_PATH = "./src/test/resources/testFile.txt";
    protected static FileReader reader;
    protected static List<String> expectedList;

    @BeforeAll
    public static void init() {
        reader = new FileReader();
        expectedList = new ArrayList<>();
        expectedList.add("Line_1");
        expectedList.add("Line_2");
    }

    @Test
    void shouldReturnExspectedListThenFilePathPassed() {
        assertEquals(reader.read(FILE_PATH), expectedList);
    }
}

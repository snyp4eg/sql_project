package com.gmail.snyp4eg.task.seven.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.gmail.snyp4eg.task.seven.model.Course;
import com.gmail.snyp4eg.task.seven.model.Group;
import com.gmail.snyp4eg.task.seven.model.Student;

public class FormatterTest {
    protected static Formatter formatter;
    protected static List<Group> groups;
    protected static List<Student> students;
    protected static List<Course> courses;
    protected static String expectedGroupsList;
    protected static String expectedStudentsList;
    protected static String expectedCoursesList;

    @BeforeAll
    public static void init() {
	formatter = new Formatter();
	Group groupOne = new Group(1, "NL-01");
	Group groupTwo = new Group(2, "NL-02");
	Course courseOne = new Course(1, "Course One");
	Course courseTwo = new Course(2, "Course Two");
	Student studentOne = new Student(1, 1, "Student", "One");
	Student studentTwo = new Student(2, 1, "Studentess", "Two");
	groups = new ArrayList<>();
	students = new ArrayList<>();
	courses = new ArrayList<>();
	groups.add(groupOne);
	groups.add(groupTwo);
	courses.add(courseOne);
	courses.add(courseTwo);
	students.add(studentOne);
	students.add(studentTwo);
	expectedGroupsList = "NL-01\n" + "NL-02";
	expectedStudentsList = "Student One\n" + "Studentess Two";
	expectedCoursesList = "1 Course One\n" + "2 Course Two";
    }
    
    @Test
    void shouldReturnExpectedStringThenSourceCoursesListPassed() {
        assertEquals(formatter.getFormattedCoursesList(courses), expectedCoursesList);
    }
    
    @Test
    void shouldReturnExpectedStringThenSourceGroupsListPassed() {
        assertEquals(formatter.getFormattedGroupsList(groups), expectedGroupsList);
    }
    
    @Test
    void shouldReturnExpectedStringThenSourceStudentsListPassed() {
        assertEquals(formatter.getFormattedStudentsList(students), expectedStudentsList);
    }
}

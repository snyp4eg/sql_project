package com.gmail.snyp4eg.task.seven.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.gmail.snyp4eg.task.seven.buisness.ConnectionFactory;
import com.gmail.snyp4eg.task.seven.buisness.ConnectionProperties;
import com.gmail.snyp4eg.task.seven.exceptions.DaoException;
import com.gmail.snyp4eg.task.seven.model.Student;
import com.gmail.snyp4eg.task.seven.ui.reader.FileReader;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class StudentDaoTest {
    protected static final String PROPERTY_PATH = "./src/main/resources/sql/query.properties";
    protected static final String QUERY_PATH = "./src/test/resources/sql/StudentDaoTest.sql";
    protected static final String DATABASE_PROPERTY_PATH = "./src/test/resources/testDatabase.properties";
    protected static final String DELIMETER = "\n";
    protected static StudentDao studentDao;
    protected static ConnectionFactory connectionFactory;
    protected static ConnectionProperties connectionProperties;
    protected static FileReader fileReader;
    protected static PropertyReader reader;
    protected static String query;

    @BeforeAll
    public static void init() {
	connectionProperties = new ConnectionProperties();
	connectionFactory = new ConnectionFactory(connectionProperties.getConnectionProperties(DATABASE_PROPERTY_PATH));
	reader = new PropertyReader(PROPERTY_PATH);
	fileReader = new FileReader();
	studentDao = new StudentDao(connectionFactory, reader);
	query = getQuery();
	createTable(query);
    }

    private static void createTable(String tablesQuery) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(tablesQuery);) {
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    sqlEx.printStackTrace();
	} catch (DaoException ioEx) {
	    ioEx.printStackTrace();
	}
    }

    private static String getQuery() {
	List<String> list = fileReader.read(QUERY_PATH);
	StringJoiner stringJoiner = new StringJoiner(DELIMETER);
	list.stream().forEach(line -> {
	    stringJoiner.add(line);
	});
	return stringJoiner.toString();
    }

    @Test
    void shouldReturnExpectedCourseThenGetByIdCalled() {
	Student expectedStudent = new Student(1, 1, "John", "Dow");
	assertEquals(studentDao.getById(1), expectedStudent);
    }

    @Test
    void shouldReturnExpectedCourseListThenGetAllCalled() {
	Student studentOne = new Student(1, 1, "John", "Dow");
	Student studentTwo = new Student(2, 1, "Jane", "Dow");
	List<Student> expectedStudentsList = new ArrayList<>();
	expectedStudentsList.add(studentOne);
	expectedStudentsList.add(studentTwo);
	assertEquals(studentDao.getAll(), expectedStudentsList);
    }

    @Test
    void shouldCreateNewDataThenInsertNewCourseCalled() {
	Student addStudent = new Student(3, 1, "Adam", "Eve");
	studentDao.insert(addStudent);
	assertEquals(studentDao.getById(3), addStudent);
    }

    @Test
    void shouldUpdateExistDataThenUpdateCourseCalled() {
	Student updateStudent = new Student(1, 2, "John", "Dow");
	studentDao.update(updateStudent);
	assertEquals(studentDao.getById(1), updateStudent);
    }

    @Test
    void shouldDeleteExistDataThenDeleteCourseCalled() {
	Student deleteStudent = new Student(2, 1, "Jane", "Dow");
	studentDao.delete(deleteStudent);
	assertEquals(studentDao.getById(2), null);
    }
}

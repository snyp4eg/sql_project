package com.gmail.snyp4eg.task.seven.buisness.generate;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.gmail.snyp4eg.task.seven.buisness.ReaderFactory;
import com.gmail.snyp4eg.task.seven.model.Course;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class CourseGeneratorTest {
    protected static final String DATABASE_PROPERTY_PATH = "./src/test/resources/testDatabase.properties";
    protected static final String FILES_PATH = "./src/test/resources/pathTest/pathTest.properties";
    protected static ReaderFactory readerFactory;
    protected static CourseGenerator courseGenerator;
    protected static PropertyReader pathReader;
    
    @BeforeAll
    public static void init() {
	pathReader = new PropertyReader(FILES_PATH);
	readerFactory = new ReaderFactory(pathReader);
	courseGenerator = new CourseGenerator(readerFactory);
    }

    @Test
    void shouldReturnExpectedCourseListSizeThenGenerateMethodCalled() {
	assertEquals(3, courseGenerator.generate().size());
    }

    @Test
    void shouldReturnExpectedCourseListSizeThengenerateCoursesListForStudentCalled() {
	List<Course> courses = courseGenerator.generate();
	assertEquals(1, courseGenerator.generateCoursesListForStudent(courses).size());
    }
}

package com.gmail.snyp4eg.task.seven.buisness.generate;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.gmail.snyp4eg.task.seven.buisness.ReaderFactory;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

class GroupGeneratorTest {
    protected static final String DATABASE_PROPERTY_PATH = "./src/test/resources/testDatabase.properties";
    protected static final String FILES_PATH = "./src/test/resources/pathTest/pathTest.properties";
    protected static ReaderFactory readerFactory;
    protected static GroupGenerator groupGenerator;
    protected static PropertyReader pathReader;
    
    @BeforeAll
    public static void init() {
	pathReader = new PropertyReader(FILES_PATH);
	readerFactory = new ReaderFactory(pathReader);
	groupGenerator = new GroupGenerator(readerFactory);
    }
    
    @Test
    void shouldReturnExpectedGroupListSizeThenGenerateMethodCalled() {
	assertEquals(3, groupGenerator.generate().size());
    }
}

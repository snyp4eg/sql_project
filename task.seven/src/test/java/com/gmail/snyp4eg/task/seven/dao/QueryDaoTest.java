package com.gmail.snyp4eg.task.seven.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.StringJoiner;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.gmail.snyp4eg.task.seven.buisness.ConnectionFactory;
import com.gmail.snyp4eg.task.seven.buisness.ConnectionProperties;
import com.gmail.snyp4eg.task.seven.model.Group;
import com.gmail.snyp4eg.task.seven.ui.reader.FileReader;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class QueryDaoTest {
    protected static final String PROPERTY_PATH = "./src/main/resources/sql/query.properties";
    protected static final String QUERY_PATH = "./src/test/resources/sql/queryDaoTest.sql";
    protected static final String DATABASE_PROPERTY_PATH = "./src/test/resources/testDatabase.properties";
    protected static final String DELIMETER = "\n";
    protected static QueryDao gueryDao;
    protected static GroupDao groupDao;
    protected static ConnectionFactory connectionFactory;
    protected static ConnectionProperties connectionProperties;
    protected static FileReader fileReader;
    protected static PropertyReader queryReader;
    protected static String query;

    @BeforeAll
    public static void init() {
	connectionProperties = new ConnectionProperties();
	connectionFactory = new ConnectionFactory(connectionProperties.getConnectionProperties(DATABASE_PROPERTY_PATH));
	fileReader = new FileReader();
	queryReader = new PropertyReader(PROPERTY_PATH);
	gueryDao = new QueryDao(connectionFactory);
	groupDao = new GroupDao(connectionFactory, queryReader);
	query = getQuery();
	    }

    private static String getQuery() {
	List<String> list = fileReader.read(QUERY_PATH);
	StringJoiner stringJoiner = new StringJoiner(DELIMETER);
	list.stream().forEach(line -> {
	    stringJoiner.add(line);
	});
	return stringJoiner.toString();
    }

    @Test
    void shouldReturnExpectedGroupListThenExectStudentCountPassed() {
	Group groupOne = new Group(1, "10-ER");
	gueryDao.executeQuery(query);
	assertEquals(groupDao.getById(1), groupOne);
    }
}

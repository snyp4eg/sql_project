package com.gmail.snyp4eg.task.seven.buisness.menu;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import com.gmail.snyp4eg.task.seven.buisness.services.GroupService;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class FindAllGroupsWithLessOrEqualsStudentCountTest {
    protected static FindAllGroupsWithLessOrEqualsStudentCount findAllGroupsWithLessOrEqualsStudentCount;
    protected static GroupService groupService;
    protected static MenuReader menuReader;
    protected static InOrder inOrder;
    
    @BeforeAll
    public static void init() {
	groupService = mock(GroupService.class);
	menuReader = mock(MenuReader.class);
	findAllGroupsWithLessOrEqualsStudentCount = spy(new FindAllGroupsWithLessOrEqualsStudentCount(groupService, menuReader));
    }
    
    @Test
    void shouldInternalMethodCalledByOrderThenExecuteMethodCalled() {
	inOrder = inOrder(menuReader, groupService);
	findAllGroupsWithLessOrEqualsStudentCount.execute();
	inOrder.verify(menuReader).getSubMenuPoint(Mockito.anyString());
	inOrder.verify(groupService).findAllGroupsWithLessOrEqualsStudentCount(Mockito.anyInt());
    }
}

package com.gmail.snyp4eg.task.seven.buisness.menu;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import com.gmail.snyp4eg.task.seven.buisness.services.CourseService;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class AddStudentToCourseTest {
    protected static AddStudentToCourse addStudentToCourse;
    protected static CourseService courseService;
    protected static MenuReader menuReader;
    protected static InOrder inOrder;
    
    @BeforeAll
    public static void init() {
	courseService = mock(CourseService.class);
	menuReader = mock(MenuReader.class);
	addStudentToCourse = spy(new AddStudentToCourse(courseService, menuReader));
    }
    
    @Test
    void shouldInternalMethodCalledByOrderThenExecuteMethodCalled() {
	inOrder = inOrder(menuReader, courseService);
	addStudentToCourse.execute();
	inOrder.verify(menuReader).getSubMenuPoint(Mockito.anyString());
	inOrder.verify(courseService).addStudentToCourse(Mockito.anyInt(), Mockito.anyInt());
    }
}

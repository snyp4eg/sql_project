package com.gmail.snyp4eg.task.seven.buisness;

import static org.mockito.Mockito.*;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import com.gmail.snyp4eg.task.seven.buisness.services.CourseService;
import com.gmail.snyp4eg.task.seven.buisness.services.GroupService;
import com.gmail.snyp4eg.task.seven.buisness.services.StudentService;
import com.gmail.snyp4eg.task.seven.ui.menu.Menu;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class MenuFactoryTest {
    protected static final String QUERY_PATH = "./src/main/resources/sql/query.properties";
    protected static final String FILES_PATH = "./src/test/resources/pathTest/pathTest.properties";
    protected static Menu menu;
    protected static MenuReader menuReader;
    protected static ReaderFactory readerFactory;
    protected static MenuFactory menuFactory;
    protected static GroupService groupService;
    protected static CourseService courseService;
    protected static StudentService studentService;
    protected static InOrder inOrder;
    protected static Map<String, String> menuMap;

    @BeforeAll
    public static void init() {
	menu = mock(Menu.class);
	menuReader = mock(MenuReader.class);
	readerFactory = mock(ReaderFactory.class);
	groupService = mock(GroupService.class);
	courseService = mock(CourseService.class);
	studentService = mock(StudentService.class);
	menuFactory = spy(new MenuFactory(menu, readerFactory, groupService, courseService, studentService));
	menuMap = new HashMap<>();
	menuMap.put("1", "One");
    }
    
    @Test
    void shouldInternalMethodsPassedByOrderThenMenuFactoryPublicMethodCalled() {
	when(menuReader.getMenuMap()).thenReturn(menuMap);
	when(readerFactory.getMenuReader()).thenReturn(menuReader);
	when(readerFactory.getQueryReader()).thenReturn(new PropertyReader(QUERY_PATH));
	inOrder = inOrder(readerFactory, menu);
	menuFactory.mainMenu();
	inOrder.verify(readerFactory).getMenuReader().getMenuMap();
	inOrder.verify(menu).setTitle(Mockito.any());
	inOrder.verify(menu).addItem(Mockito.any());
	inOrder.verify(menu).execute();
    }
}

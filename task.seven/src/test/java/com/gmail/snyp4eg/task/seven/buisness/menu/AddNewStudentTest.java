package com.gmail.snyp4eg.task.seven.buisness.menu;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import com.gmail.snyp4eg.task.seven.buisness.services.StudentService;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class AddNewStudentTest {
    protected static AddNewStudent addNewStudent;
    protected static StudentService studentService;
    protected static MenuReader menuReader;
    protected static InOrder inOrder;
    
    @BeforeAll
    public static void init() {
	studentService = mock(StudentService.class);
	menuReader = mock(MenuReader.class);
	addNewStudent = spy(new AddNewStudent(studentService, menuReader));
    }
    
    @Test
    void shouldInternalMethodCalledByOrderThenExecuteMethodCalled() {
	inOrder = inOrder(menuReader, studentService);
	addNewStudent.execute();
	inOrder.verify(menuReader).getSubMenuPoint(Mockito.anyString());
	inOrder.verify(studentService).insertStudent(Mockito.anyInt(), Mockito.anyString(), Mockito.anyString());
    }
}

package com.gmail.snyp4eg.task.seven.buisness;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;

import com.gmail.snyp4eg.task.seven.buisness.services.CourseService;
import com.gmail.snyp4eg.task.seven.buisness.services.DbService;
import com.gmail.snyp4eg.task.seven.buisness.services.GroupService;
import com.gmail.snyp4eg.task.seven.buisness.services.StudentService;
import com.gmail.snyp4eg.task.seven.model.Course;
import com.gmail.snyp4eg.task.seven.model.Group;
import com.gmail.snyp4eg.task.seven.model.Student;

public class FacadeTest {
    protected static MenuFactory menuFactory;
    protected static GroupService groupService;
    protected static CourseService courseService;
    protected static StudentService studentService;
    protected static DbService dbService;
    protected static List<Student> students;
    protected static List<Group> groups;
    protected static List<Course> courses;
    protected static Facade facade;
    protected static InOrder inOrder;
    
    @BeforeAll
    public static void init() {
	students = new ArrayList<>();
	groups = new ArrayList<>();
	courses = new ArrayList<>();
	menuFactory = mock(MenuFactory.class);
	groupService = mock(GroupService.class);
	courseService = mock(CourseService.class);
	studentService = mock(StudentService.class);
	dbService = mock(DbService.class);
	facade = spy(new Facade(groupService, courseService, studentService, dbService, menuFactory));
    }
    
    @Test
    void shouldInternalMethodCalledByOrderThenGenerateDataCalled() {
	inOrder = inOrder(groupService, courseService, studentService, dbService);
	facade.fillDatabase();
	inOrder.verify(dbService).initiateTables();
	inOrder.verify(groupService).generateData();
	inOrder.verify(groupService).saveAll(groups);
	inOrder.verify(courseService).generateData();
	inOrder.verify(courseService).saveAll(courses);
	inOrder.verify(studentService).generateData();
	inOrder.verify(studentService).saveAll(students);
	inOrder.verify(courseService).assignStudentsToCourses(courses);
	
    }
    
    @Test
    void shouldInternalMethodCalledByOrderThenShowMenuCalled() {
	inOrder = inOrder(menuFactory);
	facade.showMenu();
	inOrder.verify(menuFactory).mainMenu();
	
    }
}

DROP TABLE IF EXISTS groups;
CREATE TABLE groups (
    group_id INT, 
    group_name VARCHAR(100),
    PRIMARY KEY(group_id)
);

INSERT INTO groups VALUES (1, '10-ER');
INSERT INTO groups VALUES (2, '11-TY')
DROP TABLE IF EXISTS groups;

CREATE TABLE groups (
    group_id INT PRIMARY KEY,
    group_name VARCHAR(100)
);

INSERT INTO groups VALUES (1, '10-ER');
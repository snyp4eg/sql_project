DROP TABLE IF EXISTS courses;
CREATE TABLE courses(
    course_id INT PRIMARY KEY,
    course_name VARCHAR(100),
    course_description VARCHAR(100)   
);

INSERT INTO courses VALUES (1, 'algebra', 'algebra');
INSERT INTO courses VALUES (2, 'math', 'math')
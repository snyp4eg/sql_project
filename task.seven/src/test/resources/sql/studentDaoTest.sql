DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS students;

CREATE TABLE groups (
    group_id INT, 
    group_name VARCHAR(100),
    PRIMARY KEY(group_id)
);

INSERT INTO groups VALUES (1, '10-ER');
INSERT INTO groups VALUES (2, '11-TY');

CREATE TABLE students(
    student_id INT PRIMARY KEY,
    group_id INT REFERENCES groups,
    first_name VARCHAR(100),
    last_name VARCHAR(100)
);

INSERT INTO students  VALUES (1, 1, 'John', 'Dow');
INSERT INTO students  VALUES (2, 1, 'Jane', 'Dew')
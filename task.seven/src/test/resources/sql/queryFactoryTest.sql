DROP TABLE IF EXISTS student_course;
DROP TABLE IF EXISTS courses;
DROP TABLE IF EXISTS students;
DROP TABLE IF EXISTS groups;

CREATE TABLE courses (
    course_id INT PRIMARY KEY,
    course_name VARCHAR(100),
    course_description VARCHAR(100)
);
CREATE TABLE groups (
    group_id INT PRIMARY KEY,
    group_name VARCHAR(100)
);
CREATE TABLE students (
    student_id INT PRIMARY KEY,
    group_id INT REFERENCES groups,
    first_name VARCHAR(100),
    last_name VARCHAR(100)
);
CREATE TABLE student_course (
    student_id INT REFERENCES students,
    course_id INT REFERENCES courses,
    PRIMARY KEY(student_id, course_id)
);

INSERT INTO groups VALUES (1, '10-ER');
INSERT INTO groups VALUES (2, '11-TY');
INSERT INTO groups VALUES (3, '12-BV');
INSERT INTO courses VALUES (1, 'algebra', 'algebra');
INSERT INTO courses VALUES (2, 'math', 'math');
INSERT INTO students VALUES (1, 1, 'First', 'Student');
INSERT INTO students VALUES (2, 1, 'Second', 'Student');
INSERT INTO students VALUES (3, 3, 'Third', 'Student');
INSERT INTO students VALUES (4, 2, 'Forth', 'Student');
INSERT INTO student_course VALUES (1, 1);
INSERT INTO student_course VALUES (1, 2);
INSERT INTO student_course VALUES (2, 1);
INSERT INTO student_course VALUES (3, 1);
INSERT INTO student_course VALUES (4, 1);
INSERT INTO student_course VALUES (4, 2);
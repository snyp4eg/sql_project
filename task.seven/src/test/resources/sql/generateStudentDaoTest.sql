DROP TABLE IF EXISTS student_course;
DROP TABLE IF EXISTS courses;
DROP TABLE IF EXISTS students;
DROP TABLE IF EXISTS groups;

CREATE TABLE courses (
    course_id INT PRIMARY KEY,
    course_name VARCHAR(100),
    course_description VARCHAR(100)
);
CREATE TABLE groups (
    group_id INT PRIMARY KEY,
    group_name VARCHAR(100)
);
CREATE TABLE students (
    student_id INT PRIMARY KEY,
    group_id INT REFERENCES groups,
    first_name VARCHAR(100),
    last_name VARCHAR(100)
);
CREATE TABLE student_course (
    student_id INT REFERENCES students,
    course_id INT REFERENCES courses,
    PRIMARY KEY(student_id, course_id)
);

INSERT INTO groups VALUES (1, '10-ER');
INSERT INTO groups VALUES (2, '11-TY');
INSERT INTO courses VALUES (1, 'algebra', 'algebra');
INSERT INTO courses VALUES (2, 'math', 'math');
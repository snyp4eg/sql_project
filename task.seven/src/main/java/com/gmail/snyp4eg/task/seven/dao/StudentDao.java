package com.gmail.snyp4eg.task.seven.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import com.gmail.snyp4eg.task.seven.buisness.ConnectionFactory;
import com.gmail.snyp4eg.task.seven.exceptions.DaoException;
import com.gmail.snyp4eg.task.seven.model.Student;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class StudentDao implements StudentGenericDao {
    private static final String STUDENT_ID = "student_id";
    private static final String GROUP_ID = "group_id";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String GET_BY_ID_KEY = "studentGetById";
    private static final String GET_ALL_KEY = "studentGetAll";
    private static final String INSERT_KEY = "studentInsert";
    private static final String UPDATE_KEY = "studentUpdate";
    private static final String DELETE_KEY = "studentDelete";
    private static final String GET_STUDENT_LIST_KEY = "queryStudentsByCourse";
    private ConnectionFactory connectionFactory;
    private PropertyReader propertyReader;

    public StudentDao(ConnectionFactory connectionFactory, PropertyReader propertyReader) {
	this.connectionFactory = connectionFactory;
	this.propertyReader = propertyReader;
    }

    @Override
    public Student getById(int id) {
	Student student = null;
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection
			.prepareStatement(propertyReader.getProperty(GET_BY_ID_KEY) + id);
		ResultSet result = statement.executeQuery();) {

	    if (result.next()) {
		student = new Student(result.getInt(STUDENT_ID), result.getInt(GROUP_ID), result.getString(FIRST_NAME),
			result.getString(LAST_NAME));
	    }
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
	return student;
    }

    @Override
    public List<Student> getAll() {
	List<Student> students = new ArrayList<Student>();
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(propertyReader.getProperty(GET_ALL_KEY));
		ResultSet result = statement.executeQuery();) {
	    while (result.next()) {
		Student student = new Student(result.getInt(STUDENT_ID), result.getInt(GROUP_ID),
			result.getString(FIRST_NAME), result.getString(LAST_NAME));
		students.add(student);
	    }
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
	return students;
    }

    @Override
    public void insert(Student student) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(propertyReader.getProperty(INSERT_KEY));) {
	    statement.setInt(1, student.getStudentId());
	    statement.setInt(2, student.getGroupId());
	    statement.setString(3, student.getFirstName());
	    statement.setString(4, student.getLastName());
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }

    @Override
    public void update(Student student) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(propertyReader.getProperty(UPDATE_KEY));) {
	    statement.setInt(1, student.getGroupId());
	    statement.setString(2, student.getFirstName());
	    statement.setString(3, student.getLastName());
	    statement.setInt(4, student.getStudentId());
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }

    @Override
    public void delete(Student student) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(propertyReader.getProperty(DELETE_KEY));) {
	    statement.setInt(1, student.getStudentId());
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }
    @Override
    public List<Student> getStudentsRelatedToCourse(String courseName) {
      List<Student> students = new ArrayList<>();
      try (Connection connection = connectionFactory.createConnection();
          PreparedStatement statement = connection
              .prepareStatement(propertyReader.getProperty(GET_STUDENT_LIST_KEY));) {
          statement.setString(1, courseName);
          ResultSet result = statement.executeQuery();
          while (result.next()) {
          Student student = new Student(result.getInt(STUDENT_ID), result.getInt(GROUP_ID),
              result.getString(FIRST_NAME), result.getString(LAST_NAME));
          students.add(student);
          }
      } catch (SQLException sqlEx) {
          throw new DaoException(sqlEx.getMessage(), sqlEx);
      }
      return students;
      }
}

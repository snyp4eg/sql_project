package com.gmail.snyp4eg.task.seven.buisness;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.gmail.snyp4eg.task.seven.buisness.services.CourseService;
import com.gmail.snyp4eg.task.seven.buisness.services.DbService;
import com.gmail.snyp4eg.task.seven.buisness.services.GroupService;
import com.gmail.snyp4eg.task.seven.buisness.services.StudentService;
import com.gmail.snyp4eg.task.seven.exceptions.DaoException;
import com.gmail.snyp4eg.task.seven.model.Course;
import com.gmail.snyp4eg.task.seven.model.Group;
import com.gmail.snyp4eg.task.seven.model.Student;

public class Facade {
    private static final Logger LOGGER = Logger.getLogger(Facade.class.getName());
    private static final String START = "Start generating data...";
    private static final String DONE = "Done.";
    private MenuFactory menuFactory;
    GroupService groupService;
    CourseService courseService;
    StudentService studentService;
    DbService dbService;

    public Facade(GroupService groupService, CourseService courseService, StudentService studentService, DbService dbService, MenuFactory menuFactory) {
	this.groupService = groupService;
	this.courseService = courseService;
	this.studentService = studentService;
	this.dbService = dbService;
	this.menuFactory = menuFactory;
	}
    
    public void fillDatabase() {
	print(START);
	dbService.initiateTables();
	List<Group> groups= groupService.generateData();
	groupService.saveAll(groups);
	List<Course> courses = courseService.generateData();
	courseService.saveAll(courses);
	List<Student> students = studentService.generateData();
	studentService.saveAll(students);
	courseService.assignStudentsToCourses(courses);
	print(DONE);
    }

    public void showMenu() {
	try {
	menuFactory.mainMenu();
	}catch (DaoException ex) {
	    LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
	}
    }
    
    private void print(String data) {
	System.out.println(data);
    }
}

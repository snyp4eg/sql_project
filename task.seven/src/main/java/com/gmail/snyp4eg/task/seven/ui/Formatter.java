package com.gmail.snyp4eg.task.seven.ui;

import java.util.List;
import java.util.StringJoiner;

import com.gmail.snyp4eg.task.seven.model.Course;
import com.gmail.snyp4eg.task.seven.model.Group;
import com.gmail.snyp4eg.task.seven.model.Student;

public class Formatter {
    private static final String DELIMETER = "\n";
    private static final String SPACE = " ";

    public String getFormattedGroupsList(List<Group> groups) {
	StringJoiner stringJoiner = new StringJoiner(DELIMETER);
	groups.stream().forEach(group -> {
	    stringJoiner.add(group.getGroupName());
	});
	return stringJoiner.toString();
    }

    public String getFormattedStudentsList(List<Student> students) {
	StringJoiner stringJoiner = new StringJoiner(DELIMETER);
	students.stream().forEach(student -> {
	    stringJoiner.add(student.getFirstName() + SPACE + student.getLastName());
	});
	return stringJoiner.toString();
    }   
    
    public String getFormattedCoursesList(List<Course> courses) {
	StringJoiner stringJoiner = new StringJoiner(DELIMETER);
	courses.stream().forEach(course -> {
	    stringJoiner.add(course.getCourseId() + SPACE + course.getCourseName());
	});
	return stringJoiner.toString();
    }  
}

package com.gmail.snyp4eg.task.seven.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.gmail.snyp4eg.task.seven.buisness.ConnectionFactory;
import com.gmail.snyp4eg.task.seven.exceptions.DaoException;
import com.gmail.snyp4eg.task.seven.model.Course;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class CourseDao implements CourseGenericDao {
    private static final String COURSE_ID = "course_id";
    private static final String COURSE_NAME = "course_name";
    private static final String COURSE_DESCRIPTION = "course_description";
    private static final String GET_BY_ID_KEY = "courseGetById";
    private static final String GET_ALL_KEY = "courseGetAll";
    private static final String INSERT_KEY = "courseInsert";
    private static final String UPDATE_KEY = "courseUpdate";
    private static final String DELETE_KEY = "courseDelete";
    private static final String GET_INSERT_KEY = "studentCourseInsert";
    private static final String GET_DELETE_KEY = "studentCourseDelete";
    private static final String GET_COURSES_LIST_KEY = "studentCourseStudentId";
    private ConnectionFactory connectionFactory;
    private PropertyReader reader;

    public CourseDao(ConnectionFactory connectionFactory, PropertyReader reader) {
	this.connectionFactory = connectionFactory;
	this.reader = reader;
    }

    @Override
    public Course getById(int id) {
	Course course = null;
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(reader.getProperty(GET_BY_ID_KEY) + id);
		ResultSet result = statement.executeQuery();) {

	    if (result.next()) {
		course = new Course(result.getInt(COURSE_ID), result.getString(COURSE_NAME),
			result.getString(COURSE_DESCRIPTION));
	    }
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
	return course;
    }

    @Override
    public List<Course> getAll() {
	List<Course> courses = new ArrayList<>();
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(reader.getProperty(GET_ALL_KEY));
		ResultSet result = statement.executeQuery();) {
	    while (result.next()) {
		Course course = new Course(result.getInt(COURSE_ID), result.getString(COURSE_NAME),
			result.getString(COURSE_DESCRIPTION));
		courses.add(course);
	    }
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
	return courses;
    }

    @Override
    public void insert(Course course) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(reader.getProperty(INSERT_KEY));) {
	    statement.setInt(1, course.getCourseId());
	    statement.setString(2, course.getCourseName());
	    statement.setString(3, course.getCourseDescription());
	    statement.executeUpdate();

	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }

    @Override
    public void update(Course course) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(reader.getProperty(UPDATE_KEY));) {
	    statement.setString(1, course.getCourseName());
	    statement.setString(2, course.getCourseDescription());
	    statement.setInt(3, course.getCourseId());
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }

    @Override
    public void delete(Course course) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(reader.getProperty(DELETE_KEY));) {
	    statement.setInt(1, course.getCourseId());
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }
    @Override
    public void addStudentToCourse(int studentId, int courseId) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(reader.getProperty(GET_INSERT_KEY));) {
	    statement.setInt(1, studentId);
	    statement.setInt(2, courseId);
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }
    @Override
    public void deleteStudentFromCourse(int studentId, int courseId) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(reader.getProperty(GET_DELETE_KEY));) {
	    statement.setInt(1, studentId);
	    statement.setInt(2, courseId);
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }
    @Override
    public List<Course> getCoursesList(int studentId) {
	List<Course> courseList = new ArrayList<>();
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(reader.getProperty(GET_COURSES_LIST_KEY));) {
	    statement.setInt(1, studentId);
	    ResultSet result = statement.executeQuery();

	    while (result.next()) {
		Course course = new Course(result.getInt(COURSE_ID), result.getString(COURSE_NAME),
			result.getString(COURSE_DESCRIPTION));
		courseList.add(course);
	    }
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
	return courseList;
    }
}

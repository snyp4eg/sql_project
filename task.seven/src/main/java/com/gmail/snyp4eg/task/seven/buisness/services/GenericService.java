package com.gmail.snyp4eg.task.seven.buisness.services;

import java.util.List;

public interface GenericService<T> {
    List<T> generateData();
    void saveAll(List<T> t);
}

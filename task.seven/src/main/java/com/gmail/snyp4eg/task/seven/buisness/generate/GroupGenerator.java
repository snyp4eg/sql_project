package com.gmail.snyp4eg.task.seven.buisness.generate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import com.gmail.snyp4eg.task.seven.buisness.ReaderFactory;
import com.gmail.snyp4eg.task.seven.model.Group;

public class GroupGenerator implements DataGenerator<Group> {
    private static final String GROUPS_COUNT = "groupsCount";
    private static final String HYPHEN = "-";
    private ReaderFactory readerFactory;

    public GroupGenerator(ReaderFactory readerFactory) {
	this.readerFactory = readerFactory;
    }

    @Override
    public List<Group> generate() {
	List<String> groupNameList = new ArrayList<>();
	int count = Integer.parseInt(readerFactory.getPropertyReader().getProperty(GROUPS_COUNT));
	for (int x = 0; x < count; x++) {
	    groupNameList.add((x + count) + HYPHEN + generateLetters());
	}
	List<Group> groups = new ArrayList<>();
	AtomicInteger index = new AtomicInteger();
	groupNameList.stream().forEach(groupName -> {
	    Group group = new Group(index.getAndIncrement(), groupName);
	    groups.add(group);
	});

	return groups;
    }

    private String generateLetters() {
	char[] data = { getRandomChar(), getRandomChar() };
	String letters = new String(data);
	return letters.toUpperCase();
    }

    private static char getRandomChar() {
	Random random = new Random();
	char codePoint = (char) (random.nextInt(26) + 'a');
	return (char) codePoint;
    }
}

package com.gmail.snyp4eg.task.seven.model;

import java.util.Objects;

public class Group {
  private int groupId;
  private String groupName;

  public Group(int groupId, String groupName) {
    this.groupId = groupId;
    this.groupName = groupName;
  }

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Group group = (Group) o;
    return this.groupId == group.groupId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(groupId);
  }

  @Override
  public String toString() {
    return "Group{" + "ID= " + groupId + '\'' + ", name= '" + groupName + "}";
  }

}

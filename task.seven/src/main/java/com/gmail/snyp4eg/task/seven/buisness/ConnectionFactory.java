package com.gmail.snyp4eg.task.seven.buisness;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.gmail.snyp4eg.task.seven.exceptions.DaoException;

public class ConnectionFactory {
    private static final String URL = "url";
    private static final String USER_NAME = "userName";
    private static final String PASSWORD = "password";
    private Properties properties;

    public ConnectionFactory(Properties properties) {
	this.properties = properties;
    }

    public Connection createConnection() {
	Connection connection;
	try {
	    connection = DriverManager.getConnection(properties.getProperty(URL), properties.getProperty(USER_NAME),
		    properties.getProperty(PASSWORD));
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
	return connection;
    }
}

package com.gmail.snyp4eg.task.seven.buisness;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class ConnectionProperties {
    
    public Properties getConnectionProperties(String databaseProperties) {
	Properties properties = new Properties();
	try (InputStream reader = Files.newInputStream(Paths.get(databaseProperties));) {
	    properties.load(reader);
	} catch (IOException e) {
	    e.printStackTrace();
	}	
	return properties;
    }
}

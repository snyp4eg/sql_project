package com.gmail.snyp4eg.task.seven.buisness.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.gmail.snyp4eg.task.seven.buisness.services.GroupService;
import com.gmail.snyp4eg.task.seven.model.Group;
import com.gmail.snyp4eg.task.seven.ui.Formatter;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class FindAllGroupsWithLessOrEqualsStudentCount implements MenuExecute {
    private static final String KEY = "1";
    private GroupService groupService;
    private MenuReader menuReader;
    
    public FindAllGroupsWithLessOrEqualsStudentCount(GroupService groupService, MenuReader menuReader) {
	this.groupService = groupService;
	this.menuReader = menuReader;	
    }
    
    @Override
    public void execute() {
	int count = getStudentCount();
	List<Group> groups = groupService.findAllGroupsWithLessOrEqualsStudentCount(count);
	Formatter formatter = new Formatter();
	print(formatter.getFormattedGroupsList(groups));
    }

    private int getStudentCount() {
	print(menuReader.getSubMenuPoint(KEY));
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	int choice = 0;
	try {
	    choice = Integer.parseInt(reader.readLine());
	} catch (NumberFormatException ex) {
	    ex.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return choice;
    }
    
    private void print(String result) {
	System.out.println(result);
    }
}

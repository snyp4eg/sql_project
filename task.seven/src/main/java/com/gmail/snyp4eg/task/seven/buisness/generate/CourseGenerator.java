package com.gmail.snyp4eg.task.seven.buisness.generate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.gmail.snyp4eg.task.seven.buisness.ReaderFactory;
import com.gmail.snyp4eg.task.seven.model.Course;

public class CourseGenerator implements DataGenerator<Course> {
    private static final String MAX_COURSES_PER_ONE_STUDENT_COUNT = "maxCoursesPerOneStudentCount";
    private static final String COURSES_NAMES_KEY = "CoursesNames";
    private ReaderFactory readerFactory;

    public CourseGenerator(ReaderFactory readerFactory) {
	this.readerFactory = readerFactory;
    }

    @Override
    public List<Course> generate() {
	List<String> coursesNamesList = readerFactory.getFileReader()
		.read(readerFactory.getPathReader().getProperty(COURSES_NAMES_KEY));
	List<Course> courseList = new ArrayList<>();
	AtomicInteger index = new AtomicInteger();
	coursesNamesList.stream().forEach(courseName -> {
	    Course course = new Course(index.getAndIncrement(), courseName);
	    courseList.add(course);
	});
	return courseList;
    }

    public List<Course> generateCoursesListForStudent(List<Course> courses) {
	int maxCount = Integer
		.parseInt(readerFactory.getPropertyReader().getProperty(MAX_COURSES_PER_ONE_STUDENT_COUNT));
	int randomCoursesCount = getRandomCoursesCount(1, maxCount);
	Collections.shuffle(courses);
	return courses.stream()
		.distinct()
		.limit(randomCoursesCount)
		.collect(Collectors.toList());
    }

    private int getRandomCoursesCount(int min, int max) {
	max -= min;
	return (int) (Math.random() * ++max) + min;
    }
}

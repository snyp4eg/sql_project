package com.gmail.snyp4eg.task.seven.dao;

import java.util.List;

import com.gmail.snyp4eg.task.seven.model.Course;

public interface CourseGenericDao extends GenericDao<Course> {
  void addStudentToCourse(int studentId, int courseId);
  void deleteStudentFromCourse(int studentId, int courseId);
  List<Course> getCoursesList(int studentId);
}

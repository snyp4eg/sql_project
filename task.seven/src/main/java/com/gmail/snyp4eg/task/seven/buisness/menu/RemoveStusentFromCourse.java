package com.gmail.snyp4eg.task.seven.buisness.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.gmail.snyp4eg.task.seven.buisness.services.CourseService;
import com.gmail.snyp4eg.task.seven.model.Course;
import com.gmail.snyp4eg.task.seven.ui.Formatter;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class RemoveStusentFromCourse implements MenuExecute {
    private static final String KEY = "6";
    private static final String STUDENT_ID = "4";
    private static final String DONE = "Done.";
    private CourseService courseService;
    private MenuReader menuReader;

    public RemoveStusentFromCourse(CourseService courseService, MenuReader menuReader) {
	this.courseService = courseService;
	this.menuReader = menuReader;
    }

    @Override
    public void execute() {
	int[] dataId = getStudentCourse(KEY);
	courseService.removeStudentFromCourse(dataId[0], dataId[1]);
	print(DONE);
    }

    private int[] getStudentCourse(String key) {
	Formatter formatter = new Formatter();
	print(menuReader.getSubMenuPoint(STUDENT_ID));
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	int[] dataId = new int[2];
	try {
	    dataId[0] = Integer.parseInt(reader.readLine());
	} catch (NumberFormatException ex) {
	    ex.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}

	List<Course> courses = courseService.getStudentCourses(dataId[0]);
	String courseList = formatter.getFormattedCoursesList(courses);
	print(courseList);
	print(menuReader.getSubMenuPoint(KEY));

	try {
	    dataId[1] = Integer.parseInt(reader.readLine());
	} catch (NumberFormatException ex) {
	    ex.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return dataId;
    }

    private void print(String result) {
	System.out.println(result);
    }
}

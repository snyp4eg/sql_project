package com.gmail.snyp4eg.task.seven.ui.reader;


import java.util.List;
import java.util.StringJoiner;

public class ScriptReader {
        
  public String read(List<String> list, String delimeter) {
    StringJoiner stringJoiner = new StringJoiner(delimeter);
	list.stream().forEach(line -> {
	    stringJoiner.add(line);
	});
	return stringJoiner.toString();
  }
}

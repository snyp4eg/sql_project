package com.gmail.snyp4eg.task.seven.buisness.services;

import java.util.List;

import com.gmail.snyp4eg.task.seven.buisness.generate.GroupGenerator;
import com.gmail.snyp4eg.task.seven.dao.GroupDao;
import com.gmail.snyp4eg.task.seven.exceptions.DaoException;
import com.gmail.snyp4eg.task.seven.model.Group;

public class GroupService implements GenericService<Group> {
    private GroupGenerator generateGroups;
    private GroupDao groupDao;

    public GroupService(GroupGenerator generateGroups, GroupDao groupDao) {
	this.generateGroups = generateGroups;
	this.groupDao = groupDao;
    }

    @Override
    public List<Group> generateData() {
	return generateGroups.generate();
    }

    @Override
    public void saveAll(List<Group> groups) {
	groups.stream().forEach(group -> {
	    groupDao.insert(group);
	});
    }

    public List<Group> getAll() {
	return groupDao.getAll();
    }
    
    public List<Group> findAllGroupsWithLessOrEqualsStudentCount(int count) {
	List<Group> groups = null;
	try {
	    groups = groupDao.getGroupListByStudentCount(count);
	} catch (DaoException daoEx) {
	    throw new DaoException(daoEx.getMessage(), daoEx);
	}
	return groups;
    }
}

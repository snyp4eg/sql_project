package com.gmail.snyp4eg.task.seven.buisness.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.gmail.snyp4eg.task.seven.buisness.services.StudentService;
import com.gmail.snyp4eg.task.seven.model.Student;
import com.gmail.snyp4eg.task.seven.ui.Formatter;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class FindAllStudentsRelatedToCourse implements MenuExecute {
    private static final String KEY = "2";
    private StudentService studentService;
    private MenuReader menuReader;

    public FindAllStudentsRelatedToCourse(StudentService studentService, MenuReader menuReader) {
	this.studentService = studentService;
	this.menuReader = menuReader;
    }

    @Override
    public void execute() {
	String course = getCourseName(KEY);
	List<Student> students = studentService.findAllStudentsRelatedToCourse(course);
	Formatter formatter = new Formatter();
	print(formatter.getFormattedStudentsList(students));
    }

    private String getCourseName(String key) {
	print(menuReader.getSubMenuPoint(KEY));
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	String choice = null;
	try {
	    choice = reader.readLine();
	} catch (NumberFormatException ex) {
	    ex.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return choice;
    }

    private void print(String result) {
	System.out.println(result);
    }
}

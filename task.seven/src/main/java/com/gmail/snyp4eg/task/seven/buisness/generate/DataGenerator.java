package com.gmail.snyp4eg.task.seven.buisness.generate;

import java.util.List;

public interface DataGenerator<T> {
  List<T> generate();
}

package com.gmail.snyp4eg.task.seven.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.gmail.snyp4eg.task.seven.buisness.ConnectionFactory;
import com.gmail.snyp4eg.task.seven.exceptions.DaoException;
import com.gmail.snyp4eg.task.seven.model.Group;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class GroupDao implements GroupGenericDao {
    private static final String GROUP_ID = "group_id";
    private static final String GROUP_NAME = "group_name";
    private static final String GET_BY_ID_KEY = "groupGetById";
    private static final String GET_ALL_KEY = "groupGetAll";
    private static final String INSERT_KEY = "groupInsert";
    private static final String UPDATE_KEY = "groupUpdate";
    private static final String DELETE_KEY = "groupDelete";
    private static final String GET_GROUP_LIST_KEY = "queryGroupByCount";
    private ConnectionFactory connectionFactory;
    private PropertyReader propertyReader;

    public GroupDao(ConnectionFactory connectionFactory, PropertyReader propertyReader) {
	this.connectionFactory = connectionFactory;
	this.propertyReader = propertyReader;
    }

    @Override
    public Group getById(int id) {
	Group group = null;
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection
			.prepareStatement(propertyReader.getProperty(GET_BY_ID_KEY) + id);
		ResultSet result = statement.executeQuery();) {

	    if (result.next()) {
		group = new Group(result.getInt(GROUP_ID), result.getString(GROUP_NAME));
	    }
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
	return group;
    }

    @Override
    public List<Group> getAll() {
	List<Group> groups = new ArrayList<>();
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(propertyReader.getProperty(GET_ALL_KEY));
		ResultSet result = statement.executeQuery();) {
	    while (result.next()) {
		Group group = new Group(result.getInt(GROUP_ID), result.getString(GROUP_NAME));
		groups.add(group);
	    }
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
	return groups;
    }

    @Override
    public void insert(Group group) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(propertyReader.getProperty(INSERT_KEY));) {
	    statement.setInt(1, group.getGroupId());
	    statement.setString(2, group.getGroupName());
	    statement.executeUpdate();

	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }

    @Override
    public void update(Group group) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(propertyReader.getProperty(UPDATE_KEY));) {
	    statement.setString(1, group.getGroupName());
	    statement.setInt(2, group.getGroupId());
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }

    @Override
    public void delete(Group group) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(propertyReader.getProperty(DELETE_KEY));) {
	    statement.setInt(1, group.getGroupId());
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }
    @Override
    public List<Group> getGroupListByStudentCount(int studentCount) {
      List<Group> groups = new ArrayList<>();
      try (Connection connection = connectionFactory.createConnection();
          PreparedStatement statement = connection
              .prepareStatement(propertyReader.getProperty(GET_GROUP_LIST_KEY));) {
          statement.setInt(1, studentCount);
          ResultSet result = statement.executeQuery();
          while (result.next()) {
          Group group = new Group(result.getInt(GROUP_ID), result.getString(GROUP_NAME));
          groups.add(group);
          }
      } catch (SQLException sqlEx) {
          throw new DaoException(sqlEx.getMessage(), sqlEx);
      }
      return groups;
      }
}

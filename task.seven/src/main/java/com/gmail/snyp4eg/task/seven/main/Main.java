package com.gmail.snyp4eg.task.seven.main;

import java.io.IOException;
import java.util.logging.LogManager;

import com.gmail.snyp4eg.task.seven.buisness.ConnectionFactory;
import com.gmail.snyp4eg.task.seven.buisness.ConnectionProperties;
import com.gmail.snyp4eg.task.seven.buisness.Facade;
import com.gmail.snyp4eg.task.seven.buisness.MenuFactory;
import com.gmail.snyp4eg.task.seven.buisness.ReaderFactory;
import com.gmail.snyp4eg.task.seven.buisness.generate.CourseGenerator;
import com.gmail.snyp4eg.task.seven.buisness.generate.GroupGenerator;
import com.gmail.snyp4eg.task.seven.buisness.generate.StudentGenerator;
import com.gmail.snyp4eg.task.seven.buisness.services.CourseService;
import com.gmail.snyp4eg.task.seven.buisness.services.DbService;
import com.gmail.snyp4eg.task.seven.buisness.services.GroupService;
import com.gmail.snyp4eg.task.seven.buisness.services.StudentService;
import com.gmail.snyp4eg.task.seven.dao.CourseDao;
import com.gmail.snyp4eg.task.seven.dao.GroupDao;
import com.gmail.snyp4eg.task.seven.dao.QueryDao;
import com.gmail.snyp4eg.task.seven.dao.StudentDao;
import com.gmail.snyp4eg.task.seven.ui.menu.Menu;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;

public class Main {

    public static void main(String[] args) {
	try {
	    LogManager.getLogManager().readConfiguration(Facade.class.getResourceAsStream("/logging.properties"));
	} catch (IOException e) {
	    System.err.println("Could not setup logger configuration: " + e.toString());
	}
	String filePath = "./src/main/resources/path/filePath.properties";
	String databaseProperties = "database.properties";
	PropertyReader pathReader = new PropertyReader(filePath);
	ReaderFactory readerFactory = new ReaderFactory(pathReader);
	ConnectionProperties connectionProperties = new ConnectionProperties();
	ConnectionFactory connectionFactory = new ConnectionFactory(connectionProperties.getConnectionProperties(databaseProperties));
	Menu menu = new Menu();
	GroupGenerator generateGroups = new GroupGenerator(readerFactory);
	CourseGenerator generateCourses = new CourseGenerator(readerFactory);
	StudentGenerator generateStudents = new StudentGenerator(readerFactory);
	GroupDao groupDao = new GroupDao(connectionFactory, readerFactory.getQueryReader());
	CourseDao courseDao = new CourseDao(connectionFactory, readerFactory.getQueryReader());
	StudentDao studentDao = new StudentDao(connectionFactory, readerFactory.getQueryReader());
	QueryDao queryDao = new QueryDao(connectionFactory);
	GroupService groupService = new GroupService(generateGroups, groupDao);
	StudentService studentService = new StudentService(generateStudents, groupService, studentDao);
	CourseService courseService = new CourseService(generateCourses, studentService, courseDao);
	MenuFactory menuFactory = new MenuFactory(menu, readerFactory, groupService, courseService, studentService);
	DbService dbService = new DbService(readerFactory, queryDao);
	Facade facade = new Facade(groupService, courseService, studentService, dbService, menuFactory);

	facade.fillDatabase();
	facade.showMenu();
    }
}

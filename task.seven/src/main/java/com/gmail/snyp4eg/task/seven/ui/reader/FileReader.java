package com.gmail.snyp4eg.task.seven.ui.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileReader {

    public List<String> read(String fileName) {
	List<String> list = new ArrayList<>();
	try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
	    list = stream.collect(Collectors.toList());
	} catch (IOException ioEx) {
	    ioEx.printStackTrace();
	}
	return list;
    }
}

package com.gmail.snyp4eg.task.seven.buisness;

import com.gmail.snyp4eg.task.seven.ui.reader.FileReader;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;
import com.gmail.snyp4eg.task.seven.ui.reader.PropertyReader;
import com.gmail.snyp4eg.task.seven.ui.reader.ScriptReader;

public class ReaderFactory {
    private static final String QUERY_PATH = "queryPath";
    private static final String GENERATE_DATA_PATH = "generateDataPath";
    private PropertyReader propertyReader;

    public ReaderFactory(PropertyReader propertyReader) {
	this.propertyReader = propertyReader;
    }

    public FileReader getFileReader() {
	return new FileReader();
    }
    
    public ScriptReader getScriptReader() {
	return new ScriptReader();
    }
    
    public PropertyReader getPathReader() {
	return propertyReader;
    }
    
    public PropertyReader getQueryReader() {
	return new PropertyReader(propertyReader.getProperty(QUERY_PATH));
    }
    
    public PropertyReader getPropertyReader() {
	return new PropertyReader(propertyReader.getProperty(GENERATE_DATA_PATH));
    }
    
    public MenuReader getMenuReader() {
	return new MenuReader(propertyReader);
    }
}

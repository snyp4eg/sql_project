package com.gmail.snyp4eg.task.seven.buisness.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.gmail.snyp4eg.task.seven.buisness.services.StudentService;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class AddNewStudent implements MenuExecute {
    private static final String KEY = "3";
    private static final String DONE = "Done.";
    private static final String COMMA = ",";
    private StudentService studentService;
    private MenuReader menuReader;
    
    public AddNewStudent(StudentService studentService, MenuReader menuReader) {
	this.studentService = studentService;
	this.menuReader = menuReader;	
    }
    
    @Override
    public void execute() {
	String[] studentData = getNewStudentData(KEY);
	studentService.insertStudent(Integer.parseInt(studentData[0]), studentData[1], studentData[2]);
	print(DONE);
    }

    private String[] getNewStudentData(String key) {
	print(menuReader.getSubMenuPoint(KEY));
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	String choice = null;
	try {
	    choice = reader.readLine();
	} catch (NumberFormatException ex) {
	    ex.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return choice.split(COMMA);
    }

    private void print(String result) {
	System.out.println(result);
    }

}

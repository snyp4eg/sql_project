package com.gmail.snyp4eg.task.seven.buisness.generate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import com.gmail.snyp4eg.task.seven.buisness.ReaderFactory;
import com.gmail.snyp4eg.task.seven.model.Student;

public class StudentGenerator implements DataGenerator<Student> {
    private static final String STUDENTS_FIRST_NAMES_KEY = "StudentsFirstNames";
    private static final String STUDENTS_LAST_NAMES_KEY = "StudentsLastNames";
    private static final String STUDENTS_COUNT = "studentsCount";
    private ReaderFactory readerFactory;

    public StudentGenerator(ReaderFactory readerFactory) {
	this.readerFactory = readerFactory;
    }

    @Override
    public List<Student> generate() {
	List<String> firstNamesList = readerFactory.getFileReader()
		.read(readerFactory.getPathReader().getProperty(STUDENTS_FIRST_NAMES_KEY));
	List<String> lastNamesList = readerFactory.getFileReader()
		.read(readerFactory.getPathReader().getProperty(STUDENTS_LAST_NAMES_KEY));
	List<Student> students = new ArrayList<>();
	AtomicInteger index = new AtomicInteger();
	int count = Integer.parseInt(readerFactory.getPropertyReader().getProperty(STUDENTS_COUNT));
	IntStream.range(0, count).forEach(range -> {
	    Student student = new Student(index.getAndIncrement(), 0, getRandomNameFromList(firstNamesList),
		    getRandomNameFromList(lastNamesList));
	    students.add(student);
	});
	return students;
    }

    private String getRandomNameFromList(List<String> list) {
	Random random = new Random();
	return list.get(random.nextInt(list.size()));
    }
}

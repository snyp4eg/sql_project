package com.gmail.snyp4eg.task.seven.buisness.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.gmail.snyp4eg.task.seven.buisness.services.CourseService;
import com.gmail.snyp4eg.task.seven.model.Course;
import com.gmail.snyp4eg.task.seven.ui.Formatter;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class AddStudentToCourse implements MenuExecute {
    private static final String KEY = "5";
    private static final String DONE = "Done.";
    private static final String COMMA = ",";
    private CourseService courseService;
    private MenuReader menuReader;

    public AddStudentToCourse(CourseService courseService, MenuReader menuReader) {
	this.courseService = courseService;
	this.menuReader = menuReader;	
    }
    
    @Override
    public void execute() {
	String[] dataId = getStudentCourse(KEY);
	courseService.addStudentToCourse(Integer.parseInt(dataId[0]), Integer.parseInt(dataId[1]));
	print(DONE);
    }

    private String[] getStudentCourse(String key) {
	Formatter formatter = new Formatter();
	List<Course> courses = courseService.getAllCourses();
	String courseList = formatter.getFormattedCoursesList(courses);
	print(courseList);
	print(menuReader.getSubMenuPoint(key));
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	
	String dataIdString = null;
	try {
	    dataIdString = reader.readLine();
	} catch (NumberFormatException ex) {
	    ex.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return dataIdString.split(COMMA);	
    }

    private void print(String result) {
	System.out.println(result);
    }
}

package com.gmail.snyp4eg.task.seven.dao;

import java.util.List;

import com.gmail.snyp4eg.task.seven.model.Group;

public interface GroupGenericDao extends GenericDao<Group>{
  List<Group> getGroupListByStudentCount(int studentCount);
}

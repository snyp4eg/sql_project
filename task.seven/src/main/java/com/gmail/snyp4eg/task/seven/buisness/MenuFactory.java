package com.gmail.snyp4eg.task.seven.buisness;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.gmail.snyp4eg.task.seven.buisness.menu.AddNewStudent;
import com.gmail.snyp4eg.task.seven.buisness.menu.AddStudentToCourse;
import com.gmail.snyp4eg.task.seven.buisness.menu.DeleteStudent;
import com.gmail.snyp4eg.task.seven.buisness.menu.FindAllGroupsWithLessOrEqualsStudentCount;
import com.gmail.snyp4eg.task.seven.buisness.menu.FindAllStudentsRelatedToCourse;
import com.gmail.snyp4eg.task.seven.buisness.menu.MenuExecute;
import com.gmail.snyp4eg.task.seven.buisness.menu.RemoveStusentFromCourse;
import com.gmail.snyp4eg.task.seven.buisness.services.CourseService;
import com.gmail.snyp4eg.task.seven.buisness.services.GroupService;
import com.gmail.snyp4eg.task.seven.buisness.services.StudentService;
import com.gmail.snyp4eg.task.seven.ui.menu.Menu;
import com.gmail.snyp4eg.task.seven.ui.menu.MenuItem;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class MenuFactory {
    private static final String KEY_1 = "1";
    private static final String KEY_2 = "2";
    private static final String KEY_3 = "3";
    private static final String KEY_4 = "4";
    private static final String KEY_5 = "5";
    private static final String KEY_6 = "6";
    private static final String EXECUTE = "execute";
    private static final String MAIN_MENU = "*** My Main Menu ***";
    Menu menu;
    ReaderFactory readerFactory;
    GroupService groupService;
    CourseService courseService;
    StudentService studentService;

    public MenuFactory(Menu menu, ReaderFactory readerFactory, GroupService groupService, CourseService courseService, StudentService studentService) {
	this.menu = menu;
	this.readerFactory = readerFactory;
	this.groupService = groupService;
	this.courseService = courseService;
	this.studentService = studentService;
    }

    public void mainMenu() {
	MenuReader menuReader = readerFactory.getMenuReader();
	Map<String, String> menuMap = menuReader.getMenuMap();
	Map<String, MenuExecute> objectMap = getObjectMap();
	Set<String> keys = menuMap.keySet();
	menu.setTitle(MAIN_MENU);
	keys.stream().forEach(key -> {
	    menu.addItem(new MenuItem(menuMap.get(key), objectMap.get(key), EXECUTE));
	});
	menu.execute();
    }

    private Map<String, MenuExecute> getObjectMap() {
	Map<String, MenuExecute> objectMap = new HashMap<>();
	MenuExecute findAllGroupsWithLessOrEqualsStudentCount = new FindAllGroupsWithLessOrEqualsStudentCount(
		groupService, readerFactory.getMenuReader());
	MenuExecute findAllStudentsRelatedToCourse = new FindAllStudentsRelatedToCourse(studentService,
		readerFactory.getMenuReader());
	MenuExecute addNewStudent = new AddNewStudent(studentService, readerFactory.getMenuReader());
	MenuExecute deleteStudent = new DeleteStudent(studentService, readerFactory.getMenuReader());
	MenuExecute addStudentToCourse = new AddStudentToCourse(courseService, readerFactory.getMenuReader());
	MenuExecute removeStusentFromCourse = new RemoveStusentFromCourse(courseService, readerFactory.getMenuReader());
	objectMap.put(KEY_1, findAllGroupsWithLessOrEqualsStudentCount);
	objectMap.put(KEY_2, findAllStudentsRelatedToCourse);
	objectMap.put(KEY_3, addNewStudent);
	objectMap.put(KEY_4, deleteStudent);
	objectMap.put(KEY_5, addStudentToCourse);
	objectMap.put(KEY_6, removeStusentFromCourse);
	return objectMap;
    }
}

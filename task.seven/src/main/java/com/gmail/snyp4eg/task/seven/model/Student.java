package com.gmail.snyp4eg.task.seven.model;

import java.util.Objects;

public class Student {
  private int studentId;
  private int groupId;
  private String firstName;
  private String lastName;

  public Student(int studentId, int groupId, String firstName, String lastName) {
    this.studentId = studentId;
    this.groupId = groupId;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public int getStudentId() {
    return studentId;
  }

  public String getFirstName() {
    return firstName;
  }

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }

  public String getLastName() {
    return lastName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Student student = (Student) o;
    return this.studentId == student.studentId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(studentId);
  }

  @Override
  public String toString() {
    return "Student{" + "ID=" + studentId + '\'' + ", name='" + firstName + " " + lastName + '\'' + ", groupId='"
        + groupId + '}';
  }
}

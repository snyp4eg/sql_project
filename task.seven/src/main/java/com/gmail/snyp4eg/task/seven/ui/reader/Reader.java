package com.gmail.snyp4eg.task.seven.ui.reader;

public interface Reader {
  public String read(String fileName);
}

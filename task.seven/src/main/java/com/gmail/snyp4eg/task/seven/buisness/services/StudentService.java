package com.gmail.snyp4eg.task.seven.buisness.services;

import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import com.gmail.snyp4eg.task.seven.buisness.generate.StudentGenerator;
import com.gmail.snyp4eg.task.seven.dao.StudentDao;
import com.gmail.snyp4eg.task.seven.model.Student;

public class StudentService implements GenericService<Student>{
private StudentGenerator generateStudents;
private GroupService groupService;
private StudentDao studentDao;

public StudentService(StudentGenerator generateStudents, GroupService groupService, StudentDao studentDao) {
    this.generateStudents = generateStudents;
    this.groupService = groupService;
    this.studentDao = studentDao;
}
    @Override
    public List<Student> generateData() {
	List<Student> students = generateStudents.generate();
	int[] groupsId = getGroupIdList();
	students.stream().forEach(student ->{
	    student.setGroupId(getRandomGroupId(groupsId));
	});
	return students;
    }

    @Override
    public void saveAll(List<Student> students) {
	students.stream().forEach(student -> {
		studentDao.insert(student);
	    });
	
    }

    public List<Student> getAll() {
	return studentDao.getAll();
    }
    
    public List<Student> findAllStudentsRelatedToCourse(String courseName) {
	List<Student> students = null;
	students = studentDao.getStudentsRelatedToCourse(courseName);
	return students;
    }
    
    public void insertStudent(int groupId, String firstName, String lastName) {
	int studentId = getNewId(studentDao.getAll());
	    Student student = new Student(studentId, groupId, firstName, lastName);
	    studentDao.insert(student);	
    }

    public void deleteStudent(int studentId) {
	Student student = studentDao.getById(studentId);
	    studentDao.delete(student);	
    }
    
    public Student getStudentById(int studentId) {
	return studentDao.getById(studentId);
    }
    
    private int[] getGroupIdList() {
	int[] groupsId = new int[groupService.getAll().size()];
	AtomicInteger index = new AtomicInteger();
	groupService.getAll().stream().forEach(group -> {
	    groupsId[index.getAndIncrement()] = group.getGroupId();
	});
	return groupsId;
    }

    private int getRandomGroupId(int[] groupIdList) {
	Random random = new Random();
	return groupIdList[random.nextInt(groupIdList.length)];
    }
    
    private int getNewId(List<Student> students) {
	Student student = students.stream()
		.max(Comparator.comparing(Student::getStudentId))
		.get();
	return (student.getStudentId() + 1);
    }
}

package com.gmail.snyp4eg.task.seven.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.gmail.snyp4eg.task.seven.buisness.ConnectionFactory;
import com.gmail.snyp4eg.task.seven.exceptions.DaoException;

public class QueryDao {
    private ConnectionFactory connectionFactory;
    
    public QueryDao(ConnectionFactory connectionFactory) {
	this.connectionFactory = connectionFactory;
	}

    public void executeQuery(String query) {
	try (Connection connection = connectionFactory.createConnection();
		PreparedStatement statement = connection.prepareStatement(query);) {
	    statement.executeUpdate();
	} catch (SQLException sqlEx) {
	    throw new DaoException(sqlEx.getMessage(), sqlEx);
	}
    }
}

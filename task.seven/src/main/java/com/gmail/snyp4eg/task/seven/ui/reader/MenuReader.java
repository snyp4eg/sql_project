package com.gmail.snyp4eg.task.seven.ui.reader;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class MenuReader {
    private static final String MENU_PATH_KEY = "MenuPath";
    private static final String SUBMENU_PATH_KEY = "SubmenuPath";
    PropertyReader propertyReader;

    public MenuReader(PropertyReader propertyReader) {
	this.propertyReader = propertyReader;
    }

    public String getSubMenuPoint(String key) {
	Map<String, String> submenu = fillMenu(propertyReader.getProperty(SUBMENU_PATH_KEY));
	return submenu.get(key);
    }

    public Map<String, String> getMenuMap() {
	return fillMenu(propertyReader.getProperty(MENU_PATH_KEY));
    }

    private Map<String, String> fillMenu(String path) {
	Map<String, String> menuMap = new HashMap<>();
	Properties properties = new Properties();
	InputStream reader;
	try {
	    reader = Files.newInputStream(Paths.get(path));
	    properties.load(reader);
	} catch (IOException ioEx) {
	    ioEx.printStackTrace();
	}
	Set<String> keys = properties.stringPropertyNames();
	keys.stream().forEach(key -> {
	    menuMap.put(key, properties.getProperty(key));
	});
	return menuMap;
    }
}

package com.gmail.snyp4eg.task.seven.buisness.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.gmail.snyp4eg.task.seven.buisness.services.StudentService;
import com.gmail.snyp4eg.task.seven.ui.reader.MenuReader;

public class DeleteStudent implements MenuExecute {
    private static final String KEY = "4";
    private static final String DONE = "Done.";
    private StudentService studentService;
    private MenuReader menuReader;

    public DeleteStudent(StudentService studentService, MenuReader menuReader) {
	this.studentService = studentService;
	this.menuReader = menuReader;
    }

    @Override
    public void execute() {
	int studentId = getStudentId(KEY);
	studentService.deleteStudent(studentId);
	print(DONE);
    }

    private int getStudentId(String key) {
	print(menuReader.getSubMenuPoint(key));
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	int choice = 0;
	try {
	    choice = Integer.parseInt(reader.readLine());
	} catch (NumberFormatException ex) {
	    ex.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return choice;
    }

    private void print(String result) {
	System.out.println(result);
    }
}

package com.gmail.snyp4eg.task.seven.buisness.services;

import java.util.List;

import com.gmail.snyp4eg.task.seven.buisness.generate.CourseGenerator;
import com.gmail.snyp4eg.task.seven.dao.CourseDao;
import com.gmail.snyp4eg.task.seven.model.Course;

public class CourseService implements GenericService<Course> {
    private CourseGenerator generateCourses;
    private StudentService studentService;
    private CourseDao courseDao;

    public CourseService(CourseGenerator generateCourses, StudentService studentService, CourseDao courseDao) {
	this.generateCourses = generateCourses;
	this.studentService = studentService;
	this.courseDao = courseDao;
    }

    @Override
    public List<Course> generateData() {
	return generateCourses.generate();
    }

    @Override
    public void saveAll(List<Course> courses) {
	courses.stream().forEach(course -> {
	    courseDao.insert(course);
	});
    }

    public List<Course> getAll() {
	return courseDao.getAll();
    }
    
    public void assignStudentsToCourses(List<Course> courses) {
	studentService.getAll().stream().forEach(student ->{
	    generateCourses.generateCoursesListForStudent(courses).stream().forEach(course ->{
		courseDao.addStudentToCourse(student.getStudentId(), course.getCourseId());
	    });
	});
    }
    
    public List<Course> getAllCourses() {
	return courseDao.getAll();
    }

    public void addStudentToCourse(int studentId, int courseId) {
	courseDao.addStudentToCourse(studentId, courseId);;
    }

    public void removeStudentFromCourse(int studentId, int courseId) {
	courseDao.deleteStudentFromCourse(studentId, courseId);	
    }

    public List<Course> getStudentCourses(int studentId) {
	return courseDao.getCoursesList(studentId);	
    }
}

package com.gmail.snyp4eg.task.seven.ui.reader;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class PropertyReader {
    private String propertyPath;

    public PropertyReader(String propertyPath) {
	this.propertyPath = propertyPath;
    }

    public String getProperty(String key) {
	Properties properties = new Properties();
	try (InputStream reader = Files.newInputStream(Paths.get(propertyPath))) {
	    properties.load(reader);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return properties.getProperty(key);
    }
}

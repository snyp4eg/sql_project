package com.gmail.snyp4eg.task.seven.buisness.services;

import java.util.List;
import java.util.StringJoiner;

import com.gmail.snyp4eg.task.seven.buisness.ReaderFactory;
import com.gmail.snyp4eg.task.seven.dao.QueryDao;

public class DbService {
    private static final String DELIMETER = "\n";
    private static final String CREATE_TABLES_SCRIPT_KEY = "CreateTablesScript";
    private ReaderFactory readerFactory;
    private QueryDao queryDao;

    public DbService(ReaderFactory readerFactory, QueryDao queryDao) {
	this.readerFactory = readerFactory;
	this.queryDao = queryDao;
    }

    public void initiateTables() {
	queryDao.executeQuery(getQuery());
    }

    private String getQuery() {
	String path = readerFactory.getPathReader().getProperty(CREATE_TABLES_SCRIPT_KEY);
	List<String> list = readerFactory.getFileReader().read(path);
	StringJoiner stringJoiner = new StringJoiner(DELIMETER);
	list.stream().forEach(line -> {
	    stringJoiner.add(line);
	});
	return stringJoiner.toString();
    }
}

package com.gmail.snyp4eg.task.seven.dao;

import java.util.List;

import com.gmail.snyp4eg.task.seven.model.Student;

public interface StudentGenericDao extends GenericDao<Student> {
  List<Student> getStudentsRelatedToCourse(String courseName);
}

package com.gmail.snyp4eg.task.seven.dao;

import java.util.List;

public interface GenericDao<T> {
  T getById(int id);

  List<T> getAll();

  void insert(T t);

  void update(T t);

  void delete(T t);
}
